package vais.product.libexample

import android.Manifest
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_record_verify.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import vais.product.libexample.api.RetrofitClient
import vais.product.voice_recognition.VaisRecorder
import vais.product.voice_recognition.model.RecorderState
import vais.product.voice_recognition.model.ValueVerify
import vais.product.voice_recognition.model.ValueVerifyModel
import vais.product.voice_recognition.model.VerifyResponse
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader
import java.util.*
import java.util.concurrent.TimeUnit


class RecordVerifyActivity : AppCompatActivity() {
    private val PERMISSIONS_REQUEST_RECORD_AUDIO = 77

    private lateinit var vaisRecorder: VaisRecorder
    private lateinit var filePath: String
    private var isRecording = false
    private var isPaused = false
    private var keyGen : ByteArray? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record_verify)
//        setSupportActionBar(toolbar2)
//        supportActionBar?.apply {
//
//            toolbar2.title = "VERIFY"
//            // show back button on toolbar
//            // on back button press, it will navigate to parent activity
//            setDisplayHomeAsUpEnabled(true)
//            setDisplayShowHomeEnabled(true)
//        }
        filePath = externalCacheDir?.absolutePath + "/audioVerify.wav"

        vaisRecorder = VaisRecorder(filePath)

        vaisRecorder.onStateChangeListener = {
            when (it) {
                RecorderState.RECORDING -> startRecording()
                RecorderState.STOP -> stopRecording()
                RecorderState.PAUSE -> pauseRecording()
            }
        }

        vaisRecorder.onVerify = {
            print(it.valueVerify)
            Log.d("vaisdebug", it.valueVerify.name)

            llProgressBar.visibility = if(it.valueVerify.name == ValueVerify.Loading.name) View.VISIBLE else View.GONE

            if(it.valueVerify.name == ValueVerify.SUCCESS.name){
                messageTextView.text = "That's you. Thanks you"
                Toast.makeText(this, "That's you. Thanks you", Toast.LENGTH_LONG).show()
            }else if (it.valueVerify.name == ValueVerify.FAIL.name){
                messageTextView.text = "Not you. Please try again"
                Toast.makeText(this, it.error, Toast.LENGTH_LONG).show()
            }
        }
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        btnPaste.setOnClickListener {
            // Paste clipboard text to edit text
            val clipData: ClipData? = clipboard.primaryClip
//            Log.d("vaisdebug", (clipData == null).toString())
//
//            clipData?.apply {
//                val textToPaste:String = this.getItemAt(0).text.toString().trim()
//                Log.d("vaisdebug",textToPaste.length.toString())
//                textInputEditText.setText(textToPaste)
//            }
            var myExternalFile = File(getExternalFilesDir("MyFileStorage"), "abc.txt")

            val filename = "abc.txt"
            myExternalFile = File(getExternalFilesDir("MyFileStorage"),filename)
            if(filename.toString()!=null && filename.toString().trim()!=""){
                var fileInputStream =FileInputStream(myExternalFile)
                var inputStreamReader: InputStreamReader = InputStreamReader(fileInputStream)
                val bufferedReader: BufferedReader = BufferedReader(inputStreamReader)
                val stringBuilder: StringBuilder = StringBuilder()
                var text: String? = null
//                while ({ text = bufferedReader.readLine(); text }() != null) {
//                    stringBuilder.append(text)
//                }
                var adds = fileInputStream.readBytes()
                fileInputStream.close()
                //Displaying data on EditText
                textInputEditText.setText(adds.toString())
                keyGen= adds
//                Toast.makeText(applicationContext,stringBuilder.toString(),Toast.LENGTH_SHORT).show()
            }

        }


        vaisRecorder.onTimeElapsed = {
            Log.e(TAG, "onCreate: time elapsed $it")
            timeTextView.text = formatTimeUnit(it * 1000)
        }

        startStopRecordingButton.setOnClickListener {

            if(textInputLayout.editText?.text.isNullOrBlank()){
                Toast.makeText(this, "Please add key code", Toast.LENGTH_SHORT).show()

            }else {
                if (!isRecording) {
                    if (ContextCompat.checkSelfPermission(
                            this,
                            Manifest.permission.RECORD_AUDIO
                        )
                        != PackageManager.PERMISSION_GRANTED
                    ) {
                        ActivityCompat.requestPermissions(
                            this,
                            arrayOf(Manifest.permission.RECORD_AUDIO),
                            PERMISSIONS_REQUEST_RECORD_AUDIO
                        )
                    } else {
                        vaisRecorder.startRecording()
                    }
                } else {
                    vaisRecorder.stopRecording()
                }
            }

        }


        pauseResumeRecordingButton.setOnClickListener {
            if (!isPaused) {
                vaisRecorder.pauseRecording()
            } else {
                vaisRecorder.resumeRecording()
            }
        }
//        showAmplitudeSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
//            if (isChecked) {
//                amplitudeTextView.text = "Amplitude : 0"
//                amplitudeTextView.visibility = View.VISIBLE
//                vaisRecorder.onAmplitudeListener = {
//                    GlobalScope.launch(Dispatchers.Main) {
//                        amplitudeTextView.text = "Amplitude : $it"
//                    }
//                }
//
//            } else {
//                vaisRecorder.onAmplitudeListener = null
//                amplitudeTextView.visibility = View.GONE
//            }
//        }

        noiseSuppressorSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            vaisRecorder.noiseSuppressorActive = isChecked
            if (isChecked)
                Toast.makeText(this, "Noise Suppressor Activated", Toast.LENGTH_SHORT).show()

        }
    }

    private fun startRecording() {
        Log.d(TAG, vaisRecorder.audioSessionId.toString())
        isRecording = true
        isPaused = false
        messageTextView.visibility = View.GONE
        recordingTextView.text = "Recording..."
        recordingTextView.visibility = View.VISIBLE
        startStopRecordingButton.text = "STOP"
        pauseResumeRecordingButton.text = "PAUSE"
        pauseResumeRecordingButton.visibility = View.VISIBLE
        noiseSuppressorSwitch.isEnabled = false
    }

    fun File.chunkedSequence(chunk: Int): Sequence<ByteArray> {
        val input = this.inputStream().buffered()
        val buffer = ByteArray(chunk)
        return generateSequence {
            val red = input.read(buffer)
            if (red >= 0) buffer.copyOf(red)
            else {
                input.close()
                null
            }
        }
    }

    private fun stopRecording() {
        isRecording = false
        isPaused = false
        recordingTextView.visibility = View.GONE
        messageTextView.visibility = View.VISIBLE
        pauseResumeRecordingButton.visibility = View.GONE
//        showAmplitudeSwitch.isChecked = false
        Toast.makeText(this, "File saved at : $filePath ", Toast.LENGTH_LONG).show()
        startStopRecordingButton.text = "START"
        noiseSuppressorSwitch.isEnabled = true
        GlobalScope.launch(Dispatchers.Main) {
            timeTextView.text = ""
        }
        if(keyGen != null)
            vaisRecorder.verify(filePath, keyGen!!)



    }

    fun verify(filePathNew : String,key: String){
        try {

            val descriptionPart = RequestBody.create(MultipartBody.FORM, key)
            val fileUpload = File(filePathNew)
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileUpload)
            val bodyAudio = MultipartBody.Part.createFormData("audios", fileUpload.name, requestFile)
            RetrofitClient.instance.verify(descriptionPart,bodyAudio)
                .enqueue(object: Callback<VerifyResponse>{
                    override fun onFailure(call: Call<VerifyResponse>, t: Throwable) {
                        Log.d("vaisdebug", t.message.toString())


                    }

                    override fun onResponse(call: Call<VerifyResponse>, response: Response<VerifyResponse>) {
                        if(response.isSuccessful && response.body().dataVerifyResponse?.verify != null){
                            Log.d("vaisdebug", "alo")
                            Log.d("vaisdebug", response.body().dataVerifyResponse?.utt.toString())

                            Log.d("vaisdebug", response.body().dataVerifyResponse?.verify.toString())


                            if(response.body().dataVerifyResponse?.verify!!){

                            }else{

                            }

                        }else{

                        }


                    }

                })
        }catch (e: Exception ){
            Log.d("vaisdebug", e.message.toString())


        }

    }




    override fun onDestroy() {
        super.onDestroy()
        vaisRecorder.stopRecording()
    }

    private fun pauseRecording() {
        recordingTextView.text = "PAUSE"
        pauseResumeRecordingButton.text = "RESUME"
        isPaused = true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSIONS_REQUEST_RECORD_AUDIO -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    vaisRecorder.startRecording()
                }
                return
            }

            else -> {
            }
        }
    }

    companion object {
        private const val TAG = "MainActivity"
    }

    private fun formatTimeUnit(timeInMilliseconds: Long): String {
        return try {
            String.format(
                Locale.getDefault(),
                "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(timeInMilliseconds),
                TimeUnit.MILLISECONDS.toSeconds(timeInMilliseconds) - TimeUnit.MINUTES.toSeconds(
                    TimeUnit.MILLISECONDS.toMinutes(timeInMilliseconds)
                )
            )
        } catch (e: Exception) {
            "00:00"
        }
    }
}
