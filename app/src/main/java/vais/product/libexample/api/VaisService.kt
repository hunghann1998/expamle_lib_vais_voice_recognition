package vais.product.libexample.api

import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import vais.product.voice_recognition.model.GenKeyResponse
import vais.product.voice_recognition.model.VerifyResponse


interface VaisService {
    @Multipart
    @POST("api/sre/verify/check")
    fun verify(@Part("encode_key") encodeKey: RequestBody,@Part  audio: MultipartBody.Part ): Call<VerifyResponse>


    @Multipart
    @POST("api/sre/verify/extract")
    fun genKey(@Part audio: MultipartBody.Part): Call<GenKeyResponse>


}