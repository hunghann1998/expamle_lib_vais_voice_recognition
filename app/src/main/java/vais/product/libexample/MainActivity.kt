package vais.product.libexample

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity: AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnVerify.setOnClickListener{
            val intent: Intent = Intent (this, RecordVerifyActivity::class.java)
            startActivity(intent)
        }

        btnGenkey.setOnClickListener{
            val intent: Intent = Intent (this, RecordGenKeyActivity::class.java)
            startActivity(intent)
        }

    }


}