package vais.product.libexample.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import vais.product.libexample.R
import vais.product.libexample.model.CustomModel

class CustomAdapter(
    val clickListener: (Int) -> Unit) : ListAdapter<CustomModel, CustomAdapter.NoteVH>(CustomDiffCallBack()) {
    override fun onBindViewHolder(holder: NoteVH, position: Int) {

        holder.bindData(getItem(position), clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteVH {
        val inflater = LayoutInflater.from(parent.context)
        return NoteVH(inflater.inflate(R.layout.text_row_item, parent, false))
    }

    class NoteVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val content: TextView = itemView.findViewById(R.id.txtContent)
        private val stt: TextView = itemView.findViewById(R.id.txtSTT)

        fun bindData(customModel: CustomModel, clickListener: (Int) -> Unit) {
            content.text = customModel.content
            stt.text = customModel.id.toString()
        }
    }
}