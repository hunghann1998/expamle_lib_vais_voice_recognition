package vais.product.libexample.util

import android.util.Log
import vais.product.voice_recognition.model.ItemVector
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*


fun toByteArray(nmbs: Array<FloatArray>): ByteArray? {
    return try {
        val nmbsBytes =
            ByteArray((nmbs.size * nmbs[0].size * 4) )
        var k = 0
        for (element in nmbs) {
            for (element2 in element) {
                val array: ByteArray = getBytes(element2)
                for (m in array.indices) {
                    nmbsBytes[k++] = array[m]
                }
            }
        }


        var byteArrayWithTotalFloatArray = ByteArray(nmbsBytes.size + 1)
        byteArrayWithTotalFloatArray = nmbsBytes.copyOf()
        byteArrayWithTotalFloatArray += nmbs.size.toByte()
        byteArrayWithTotalFloatArray
//        nmbsBytes
    } catch (e: java.lang.Exception) {
        Log.d("vaisdebug", e.toString())
        null
    }
}

fun getBytes(value: Float): ByteArray {
    val buffer =
        ByteBuffer.allocate(4).order(ByteOrder.nativeOrder())
    buffer.putFloat(value)
    return buffer.array()
}

fun byteToFloat(input: ByteArray): FloatArray? {

    try{
        val ret = FloatArray(input.size / 4)
        var x = 0
        while (x < input.size) {
            ret[x / 4] = ByteBuffer.wrap(input, x, 4).order(ByteOrder.nativeOrder()).float
            x += 4
        }
        return ret
    }catch(e :Exception){
        return null
    }
}

fun arrayByteTo2Darray(value: ByteArray): Array<FloatArray>?{
  try{
      val totalFloatArray = value.last()
      Log.d("kiemtra","$totalFloatArray")
      val realByteArray = value.copyOfRange(0,value.size - 1)
      Log.d("kiemtra","${realByteArray.size}")

      val dataFloatArray = byteToFloat(realByteArray)
      Log.d("kiemtra","${dataFloatArray!!.size}")


      var dataReturn : MutableList<FloatArray> = ArrayList()
      for (i in 0 until totalFloatArray.toInt()) {
          val u = dataFloatArray.copyOfRange((i * (dataFloatArray.size / totalFloatArray )),((i + 1) * (dataFloatArray.size / totalFloatArray)))
          dataReturn.add(u)
      }

      return dataReturn.toTypedArray()
  }catch(e:Exception){
      return null
  }
}



fun countByte(array: ArrayList<ItemVector>) : Int{
    var count:Int = 0
    array.forEach {
        count += (it.vector!!.size * 4)
    }
    return count
}

fun getNumberVector(array: ArrayList<ItemVector>): Int {
    if(countByte(array) > 2750)
        return 1
    return array.size
}






