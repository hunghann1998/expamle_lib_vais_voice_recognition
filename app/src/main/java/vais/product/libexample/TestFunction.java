package vais.product.libexample;

import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class TestFunction {
    public static byte[] ToByteArray(float[][] nmbs) {
       try{
           float[] arr2 = new float[]{(float) 1.0, (float) 2.0, (float) 3.0};
           float[][] var10000 = new float[][]{arr2,arr2};
           Log.d("vaisdebug",nmbs + "");

           Log.d("vaisdebug",nmbs[0].length + "");
           Log.d("vaisdebug",nmbs[1].length + "");
//           return null;

           byte[] nmbsBytes = new byte[nmbs[0].length * nmbs[1].length * 4];
           int k = 0;

           for(int i = 0; i < nmbs[0].length; ++i) {
               for(int j = 0; j < nmbs[1].length; ++j) {
                   byte[] array = GetBytes(nmbs[i][j]);

                   for(int m = 0; m < array.length; ++m) {
                       nmbsBytes[k++] = array[m];
                   }
               }
           }

           return nmbsBytes;
       }catch (Exception e){
           Log.d("vaisdebug",e.toString());
           return null;
       }
    }

    public static byte[] GetBytes(float value) {
        ByteBuffer buffer = ByteBuffer.allocate(4).order(ByteOrder.nativeOrder());
        buffer.putFloat(value);
        return buffer.array();
    }
}
