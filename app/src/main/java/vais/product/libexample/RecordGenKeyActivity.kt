package vais.product.libexample

import android.Manifest
import android.content.ClipboardManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_record_genkey.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import vais.product.libexample.adapter.CustomAdapter
import vais.product.libexample.model.CustomModel
import vais.product.libexample.model.DataVerifyRequest
import vais.product.libexample.util.arrayByteTo2Darray
import vais.product.libexample.util.toByteArray
import vais.product.voice_recognition.VaisRecorder
import vais.product.voice_recognition.model.*
import java.io.*
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*
import java.util.concurrent.TimeUnit

class RecordGenKeyActivity : AppCompatActivity() {
    private val PERMISSIONS_REQUEST_RECORD_AUDIO = 77

    private lateinit var vaisRecorder: VaisRecorder
    private lateinit var filePath: String
    private var isRecording = false
    private var isPaused = false
    private var totalAudioRecord = 0
    var keyGen : ByteArray? = null

    private var dataList = mutableListOf<CustomModel>(

    )

    private var customAdapter = CustomAdapter() { position: Int ->
        deleteCustomModel(position)
    }

    private fun deleteCustomModel(position: Int) {
        dataList.removeAt(position)
        customAdapter.notifyItemRemoved(position)
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record_genkey)


        customAdapter.submitList(dataList)
        lstAudioUser.apply {
            layoutManager = LinearLayoutManager(this@RecordGenKeyActivity)
            adapter = customAdapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
//



        filePath = externalCacheDir?.absolutePath + "/audio_genkey_${totalAudioRecord + 1}.wav"

        vaisRecorder = VaisRecorder(filePath)

        vaisRecorder.onStateChangeListener = {
            when (it) {
                RecorderState.RECORDING -> startRecording()
                RecorderState.STOP -> stopRecording()
                RecorderState.PAUSE -> pauseRecording()
            }
        }

        vaisRecorder.onGenKey = {
            Log.d("vaisdebug", it.genkeyState.name)

            llProgressBar2.visibility = if(it.genkeyState.name == GenkeyState.Loading.name) View.VISIBLE else View.GONE

            if(it.genkeyState.name == ValueVerify.SUCCESS.name){
                messageTextView.visibility = View.GONE
                btnGenKey.visibility = View.GONE
                startStopRecordingButton.visibility = View.GONE
                layoutGenKey.visibility = View.VISIBLE

                txtKeyGen.text =  it.value?.toString()
                keyGen = it.value!!


                dataList.clear();
                customAdapter.submitList(dataList)
                customAdapter.notifyDataSetChanged()

                Toast.makeText(this, "Success. Copy your key", Toast.LENGTH_SHORT).show()


            }else if (it.genkeyState.name == ValueVerify.FAIL.name){
                Log.d("vaisdebug", "e2242")
                Toast.makeText(this, it.error, Toast.LENGTH_SHORT).show()
            }
        }


        vaisRecorder.onTimeElapsed = {
            Log.e(TAG, "onCreate: time elapsed $it")
            timeTextView.text = formatTimeUnit(it * 1000)
        }

        startStopRecordingButton.setOnClickListener {

            if (!isRecording) {
                if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.RECORD_AUDIO
                    )
                    != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.RECORD_AUDIO),
                        PERMISSIONS_REQUEST_RECORD_AUDIO
                    )
                } else {
                    filePath = externalCacheDir?.absolutePath + "/audio_genkey_${totalAudioRecord + 1}.wav"
                    vaisRecorder.changeFilePath(filePath)
                    vaisRecorder.startRecording()
                }
            } else {
                vaisRecorder.stopRecording()
            }
        }
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        txtCopy.setOnClickListener {

            writeBytesAsPdf(keyGen!!)
            Toast.makeText(this, "Copy Done", Toast.LENGTH_SHORT).show()

        }


        pauseResumeRecordingButton.setOnClickListener {
            if (!isPaused) {
                vaisRecorder.pauseRecording()
            } else {
                vaisRecorder.resumeRecording()
            }
        }
//        showAmplitudeSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
//            if (isChecked) {
//                amplitudeTextView.text = "Amplitude : 0"
//                amplitudeTextView.visibility = View.VISIBLE
//                vaisRecorder.onAmplitudeListener = {
//                    GlobalScope.launch(Dispatchers.Main) {
//                        amplitudeTextView.text = "Amplitude : $it"
//                    }
//                }
//
//            } else {
//                vaisRecorder.onAmplitudeListener = null
//                amplitudeTextView.visibility = View.GONE
//            }
//        }

        noiseSuppressorSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            vaisRecorder.noiseSuppressorActive = isChecked
            if (isChecked)
                Toast.makeText(this, "Noise Suppressor Activated", Toast.LENGTH_SHORT).show()

        }

        btnGenKey.setOnClickListener{
            var listPath = arrayListOf<String>()
            dataList.forEach {
                listPath.add(it.content)
            }
            vaisRecorder.genKey(listPath)
        }

    }
    fun genKey(listPathAudio : List<String>){
        try{
            val builder = MultipartBody.Builder()
            builder.setType(MultipartBody.FORM)
//        builder.addFormDataPart("user_name", "Robert")
            for (i in listPathAudio.indices) {
                val file = File(listPathAudio[i])
                builder.addFormDataPart(
                    "audios",
                    file.name,
                    RequestBody.create(MediaType.parse("multipart/form-data"), file)
                )
            }
            val requestBody = builder.build()
            vais.product.voice_recognition.api.RetrofitClient.instance.genKey(requestBody)
                .enqueue(object: Callback<GenKeyResponse>{
                    override fun onFailure(call: Call<GenKeyResponse>, t: Throwable) {
                        Log.d("vaisdebug", t.message.toString())
                    }

                    override fun onResponse(call: Call<GenKeyResponse>, response: Response<GenKeyResponse>) {
                        if(response.isSuccessful &&  response.body().dataGenKeyResponse?.items?.size!! > 0){
                            val list: MutableList<FloatArray> = ArrayList()

                            response.body().dataGenKeyResponse?.items!!.forEach {
                                val x = it.vector!!.toFloatArray()
                                list.add(x)

                            }
                            val valueBytesArray = toByteArray(list.toTypedArray())
                            Log.d("vaisdebug", "ket qua 3 ${valueBytesArray!!.size}")

                            val newVector = arrayByteTo2Darray(valueBytesArray!!)



                            var newList = list.toTypedArray()

                            newVector!!.forEach {
                                Log.d("vaisdebug", "ket qua sized ${it.size}")
                            }
                            Log.d("vaisdebug", "ket qua cuoi ${newVector!!.contentDeepEquals(newList)}")



//                            val gson = Gson()
//                            val gsonPretty = GsonBuilder().setPrettyPrinting().create()
//                            val listF : MutableList<VectorF> = ArrayList()
//                            newVector!!.forEach {
//                                listF.add(VectorF(it.toTypedArray()))
//                            }
//                            val jsonTut: String = gson.toJson(VerifyRequest(listF))
//                            Log.d("vaisdebug", "jsonTut ${jsonTut}")

//                            val users = Arrays.asList(
//                                    User("John Doe", "john.doe@example.com", arrayOf("Member", "Admin"), true),
//                                    User("Tom Lee", "tom.lee@example.com", arrayOf("Member"), false)
//                            )

                            val listItem: MutableList<DataVerifyRequest> = ArrayList()
                            newVector!!.forEach {
                                listItem.add(DataVerifyRequest(newVector[0].toTypedArray()))
                            }


                            // convert users list to JSON array

                            // convert users list to JSON array
                            val json = Gson().toJson(listItem)
                            Log.d("vaisdebug",  ""+"{\"payload\": {\"item\": $json}}")
                        }else{

                        }


                    }

                })
        }catch(e: Exception){
            Log.d("vaisdebug", e.message.toString())

        }
    }


    fun byteToFloat(input: ByteArray): FloatArray? {
        val ret = FloatArray(input.size / 4)
        var x = 0
        while (x < input.size) {
            ret[x / 4] = ByteBuffer.wrap(input, x, 4).order(ByteOrder.nativeOrder()).float
            x += 4
        }
        return ret
    }


    fun ToByteArray(nmbs: Array<FloatArray>): ByteArray? {
        return try {
            val nmbsBytes =
                ByteArray((nmbs.size * nmbs[0].size * 4) )
            var k = 0
            for (i in 0 until nmbs.size) {
                for (j in 0 until nmbs[0].size) {


                    val array: ByteArray = GetBytes(nmbs[i][j])
                    for (m in array.indices) {
                        nmbsBytes[k++] = array[m]
                    }
                }
            }
            Log.d("vaisdebugg",nmbs.size.toByte().toString())
            Log.d("vaisdebugg",nmbsBytes.size.toString())
            nmbsBytes
//            nmbsBytes.plus(nmbs.size.toByte())
//            val x = ByteArray(nmbsBytes.size + 1)
////            x.plus(nmbs.size.toByte())
//            x.plus(nmbsBytes)
//            nmbsBytes.forEach {
//                Log.d("vaisdebug",it.toString())
//            }
//            x
        } catch (e: java.lang.Exception) {
            Log.d("vaisdebug", e.toString())
            null
        }
    }

    fun GetBytes(value: Float): ByteArray {
        val buffer =
            ByteBuffer.allocate(4).order(ByteOrder.nativeOrder())
        buffer.putFloat(value)
        return buffer.array()
    }

    fun countByte(array: ArrayList<ItemVector>) : Int{
        var count:Int = 0
        array.forEach {
            count += (it.vector!!.size * 4)
        }
        return count
    }

    fun writeBytesAsPdf(bytes : ByteArray) {
        var myExternalFile = File(getExternalFilesDir("MyFileStorage"), "abc.txt")
        try {
            val fileOutPutStream = FileOutputStream(myExternalFile)
            fileOutPutStream.write(bytes)
            fileOutPutStream.close()


            var myExternalFile = File(getExternalFilesDir("MyFileStorage"), "abc.txt")

            val filename = "abc.txt"
            myExternalFile = File(getExternalFilesDir("MyFileStorage"),filename)
            if(filename.toString()!=null && filename.toString().trim()!=""){
                var fileInputStream = FileInputStream(myExternalFile)
                var inputStreamReader: InputStreamReader = InputStreamReader(fileInputStream)
                val bufferedReader: BufferedReader = BufferedReader(inputStreamReader)
                val stringBuilder: StringBuilder = StringBuilder()
                var text: String? = null
//                while ({ text = bufferedReader.readLine(); text }() != null) {
//                    stringBuilder.append(text)
//                }
                var adds = fileInputStream.readBytes()
                Log.d("kiemtra",adds.toString())
                Log.d("kiemtra",bytes.toString())

                Log.d("kiemtra",adds.contentEquals(bytes).toString())
                fileInputStream.close()
                //Displaying data on EditText
//                textInputEditText.setText(adds.toString())
//                Toast.makeText(applicationContext,stringBuilder.toString(),Toast.LENGTH_SHORT).show()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }


    private fun startRecording() {
        Log.d(TAG, vaisRecorder.audioSessionId.toString())
        isRecording = true
        isPaused = false
        messageTextView.visibility = View.GONE
        lstAudioUser.visibility = View.GONE
        recordingTextView.text = "Recording..."
        recordingTextView.visibility = View.VISIBLE
        startStopRecordingButton.text = "STOP"
        btnGenKey.visibility = View.GONE
        pauseResumeRecordingButton.text = "PAUSE"
        pauseResumeRecordingButton.visibility = View.VISIBLE
        noiseSuppressorSwitch.isEnabled = false
    }

    private fun stopRecording() {
        isRecording = false
        isPaused = false
        totalAudioRecord += 1;

        recordingTextView.visibility = View.GONE
        messageTextView.visibility = View.VISIBLE
        lstAudioUser.visibility = View.VISIBLE
        pauseResumeRecordingButton.visibility = View.GONE
//        showAmplitudeSwitch.isChecked = false
        var newModel  = CustomModel(totalAudioRecord ,filePath)
        dataList.add(newModel);
        customAdapter.submitList(dataList);
        Toast.makeText(this, "File saved at : $filePath", Toast.LENGTH_LONG).show()
        startStopRecordingButton.text = "START"
        noiseSuppressorSwitch.isEnabled = true
        GlobalScope.launch(Dispatchers.Main) {
            timeTextView.text = ""
        }
        btnGenKey.text = "Genkey"

        btnGenKey.visibility = View.VISIBLE

    }

    override fun onDestroy() {
        super.onDestroy()
        vaisRecorder?.stopRecording()
    }

    private fun pauseRecording() {
        recordingTextView.text = "PAUSE"
        pauseResumeRecordingButton.text = "RESUME"
        isPaused = true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSIONS_REQUEST_RECORD_AUDIO -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    filePath = externalCacheDir?.absolutePath + "/audio_genkey_${totalAudioRecord + 1}.wav"
                    vaisRecorder.changeFilePath(filePath)
                    vaisRecorder.startRecording()
                }
                return
            }

            else -> {
            }
        }
    }

    companion object {
        private const val TAG = "MainActivity"
    }

    private fun formatTimeUnit(timeInMilliseconds: Long): String {
        return try {
            String.format(
                Locale.getDefault(),
                "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(timeInMilliseconds),
                TimeUnit.MILLISECONDS.toSeconds(timeInMilliseconds) - TimeUnit.MINUTES.toSeconds(
                    TimeUnit.MILLISECONDS.toMinutes(timeInMilliseconds)
                )
            )
        } catch (e: Exception) {
            "00:00"
        }
    }



}
